# PHP Yield Scheduler

#### 介绍

在PHP中使用协程实现多任务调度


#### 使用说明

* PHP >= 5.5

```shell
## 运行demo
$ php ./yieldScheduler1.php

$ php ./yieldScheduler2.php

$ php ./yieldScheduler3.php


```

