<?php

require __DIR__ . '/yieldFunctions.php';

/* ------ generator::current ------ */
echo 'eg: NO.1' . PHP_EOL;
$gen = false_yield_func1();
echo 'call false_yield_func1 return: ' . $gen  . PHP_EOL;
$gen = yield_func1();
echo 'call yield_func1 current ' . PHP_EOL;
$re = $gen->current();
echo 'get yield_func4 current return :' ;
var_export($re);
echo PHP_EOL;
$gen = yield_func4();
echo 'call yield_func4 current ' . PHP_EOL;
$re = $gen->current();
echo 'get yield_func4 current return :' ;
var_export($re);
echo PHP_EOL;

echo 'eg: NO.2' . PHP_EOL;
$gen = yield_func7();
echo 'call yield_func7 current ' . PHP_EOL;
$re = $gen->current();
echo 'get yield_func7 current return :' ;
var_export($re);
echo PHP_EOL;
$re = $gen->current();
echo 'get yield_func7 current second return :' ;
var_export($re);
echo PHP_EOL . PHP_EOL;

/* ------ generator::send ------ */
echo 'eg: NO.3' . PHP_EOL;
$gen = yield_func7();
echo 'call yield_func7 current ' . PHP_EOL;
$re = $gen->current();
echo 'get yield_func7  current return :';
var_export($re);
echo PHP_EOL;
echo 'call yield_func7 send 1 ' . PHP_EOL;
$re = $gen->send(1);
echo 'get yield_func7 send 1 return :';
var_export($re);
echo PHP_EOL;

echo 'eg: NO.4' . PHP_EOL;
$gen = yield_func7();
echo 'call yield_func7 send 1 ' . PHP_EOL;
$re = $gen->send(1);
echo 'get yield_func7 send 1 return :';
var_export($re);
echo PHP_EOL . PHP_EOL;

/* ------ generator::next ------ */
echo 'eg: NO.5' . PHP_EOL;
$gen = yield_func12();
$re = $gen->current();
echo 'get yield_func7 send 1 return :';
var_export($re);
echo PHP_EOL;
echo 'call yield_func12 next ' . PHP_EOL;
$gen->next();

echo 'eg: NO.6' . PHP_EOL;
$gen = yield_func12();
echo 'call yield_func12 next ' . PHP_EOL;
$gen->next();
echo 'call yield_func12 next second ' . PHP_EOL;
$gen->next();
echo 'call yield_func12 next third ' . PHP_EOL;
$gen->next();
echo 'call yield_func12 next fourth ' . PHP_EOL;
$gen->next();
echo 'call yield_func12 next fifth ' . PHP_EOL;
$gen->next();
$re = $gen->getReturn();
var_export($re);
echo PHP_EOL . PHP_EOL;

/* ------ generator::rewind ------ */
echo 'eg: NO.7' . PHP_EOL;
$gen = yield_func12();
echo 'call yield_func12 rewind ' . PHP_EOL;
$gen->rewind();

echo 'eg: NO.8' . PHP_EOL;
$gen = yield_func12();
echo 'call yield_func12 current ' . PHP_EOL;
$gen->current();
echo 'call yield_func12 rewind ' . PHP_EOL;
$gen->rewind();

echo 'eg: NO.9' . PHP_EOL;
$gen = yield_func12();
echo 'call yield_func12 send ' . PHP_EOL;
$gen->send('aa');
// 这里调用这句代码会导致报错
//echo 'try call yield_func12  rewind' . PHP_EOL;
//$gen->rewind();
echo 'call yield_func12 send cc ' . PHP_EOL;
$gen->send('cc');
echo 'call yield_func12 send dd ' . PHP_EOL;
$gen->send('dd');
echo 'call yield_func12 send ee ' . PHP_EOL;
$gen->send('ee');
echo 'call yield_func12  return ' . PHP_EOL;
$re = $gen->getReturn();
var_export($re);
// 这里调用该代码 也会导致报错
//echo 'try call yield_func12  rewind ' . PHP_EOL;
//$gen->rewind();
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.10' . PHP_EOL;
$gen = yield_func12();
echo 'call yield_func12 current ' . PHP_EOL;
$gen->current();
echo 'call yield_func12 rewind ' . PHP_EOL;
$gen->rewind();
echo 'call yield_func12 current ' . PHP_EOL;
$gen->next();
echo PHP_EOL . PHP_EOL;

/* ------ generator::throw ------ */
echo 'eg: NO.11' . PHP_EOL;
$gen = yield_func2();
$gen->current();
echo 'throw a exception to yield_func2 ' . PHP_EOL;
$gen->throw(new \Exception('new yield  exception'));
echo 'eg: NO.12' . PHP_EOL;
$gen = yield_func2();
$gen->send(1);
// 调用会导致未捕获的异常抛出
//$gen->throw(new \Exception('new yield  exception'));
echo PHP_EOL . PHP_EOL;

/* ------ generator::valid ------ */
echo 'eg: NO.13' . PHP_EOL;
$gen = yield_func12();
$check = $gen->valid();
echo 'the generator valid ? ' . intval($check) . PHP_EOL;
echo 'eg: NO.14' . PHP_EOL;
$gen = yield_func12();
$gen->current();
echo "the generator's current has been called" . PHP_EOL;
$check = $gen->valid();
echo 'the generator valid ? ' . intval($check) . PHP_EOL;;
$gen->send(1);
echo "the generator's send has been called" . PHP_EOL;
$check = $gen->valid();
echo 'the generator valid ? ' . intval($check) . PHP_EOL;
$gen->send(2);
$gen->send(3);
echo "the generator's send has been called twice" . PHP_EOL;
$check = $gen->valid();
echo 'the generator valid ? ' . intval($check);
echo PHP_EOL . PHP_EOL;

/* ------ generator::key ------ */
echo 'eg: NO.15' . PHP_EOL;
$gen = yield_func10();
echo 'call yield_func1 current ' . PHP_EOL;
$gen->current();
echo 'call yield_func10 key ';
$key = $gen->key();
var_export($key);
$gen->next();
echo PHP_EOL . 'call yield_func10 key ';
$key = $gen->key();
var_export($key);
$gen->next();
echo PHP_EOL . 'call yield_func10 key ';
$key = $gen->key();
var_export($key);
$gen->next();
echo PHP_EOL . 'call yield_func10 key ';
$key = $gen->key();
var_export($key);
echo PHP_EOL;

echo 'eg: NO.16' . PHP_EOL;
$gen = yield_func14();
echo 'call yield_func14 key ' . PHP_EOL;
$key = $gen->key();
echo 'called yield_func14 key ';
var_export($key);
echo PHP_EOL . PHP_EOL;


/* ------ generator::__wakeup ------ */
echo 'eg: NO.17' . PHP_EOL;
$gen = yield_func4();
try {
    $ser =serialize($gen);
} catch (\Exception $e) {
    print_r($e->getMessage());
}
echo PHP_EOL . 'eg: NO.18' . PHP_EOL;
$obj = new YieldClass1();
try {
    echo 'serialize YieldClass1 obj' . PHP_EOL;
    $ser =serialize($obj);
} catch (\Exception $e) {
    print_r($e->getMessage());
}
echo PHP_EOL . PHP_EOL;

/* ------ generator::getReturn ------ */
echo 'eg: NO.19' . PHP_EOL;
$gen = yield_func12();
echo 'call yield_func12 send ' . PHP_EOL;
$gen->send('aa');
// 没有执行到return语句时，调用该函数会导致致命错误
//echo 'call yield_func12  return ' . PHP_EOL;
//$re = $gen->getReturn();
echo 'call yield_func12 send cc ' . PHP_EOL;
$gen->send('cc');
echo 'call yield_func12 send dd ' . PHP_EOL;
$gen->send('dd');
echo 'call yield_func12 send ee ' . PHP_EOL;
$gen->send('ee');
echo 'call yield_func12  return ' . PHP_EOL;
$re = $gen->getReturn();
var_export($re);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.20' . PHP_EOL;
$gen = yield_func14();
// 函数未执行完不能调用getReturn 方法
try {
    echo 'call yield_func14 return ' . PHP_EOL;
    $re = $gen->getReturn();
} catch (\Exception $e) {
    print_r($e->getMessage() . PHP_EOL);
}
$re = $gen->next();
echo 'call yield_func14 return :' . gettype($gen->getReturn()) . PHP_EOL;

echo 'eg: NO.21' . PHP_EOL;
$gen = yield_func10();
echo 'call yield_func7 send 1 ' . PHP_EOL;
$re = $gen->send(1);
echo 'get yield_func7 send 2 return :';
$re1 = $gen->send(2);
var_export($re1);
echo PHP_EOL;
echo 'get yield_func7 current return :';
$re2 = $gen->current();
var_export($re2);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.22' . PHP_EOL;
$gen = call_user_func('yield_func4');
echo 'call yield_func4 send 1 ' . PHP_EOL;
$re = $gen->send(1);
echo 'get yield_func4  getReturn :';
$re1 = $gen->getReturn();
var_export($re1);
echo PHP_EOL . PHP_EOL;