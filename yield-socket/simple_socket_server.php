<?php

define('DS', DIRECTORY_SEPARATOR);

require __DIR__ . DS;

$host = '0.0.0.0';
$port = 8080;

$server = new SyncTcpServer($host, $port);

$server->on('accept', function(SyncTcpServer $server, $socket, $info) {
    $server->console('accept a new client~' . PHP_EOL);
});

$server->on('receive', function(SyncTcpServer $server, $socket, $info, $data) {
    $server->console('received data :' . $data . ' from : ' . strval($socket) . PHP_EOL);
    fwrite($socket, $data .  ' time is: ' . date('Y-m-d H;i:s') . PHP_EOL);
});

$server->on('close', function(SyncTcpServer $server, $socket, $info) {
    $server->console('client :' . strval($socket) . ' closed.' . PHP_EOL);
});

$server->run();