<?php


function false_yield_func1()
{
    // named with yield
    $arr = array(1,2,3);
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    return 4;
}

function yield_func1()
{
    $arr = array(1,2,3);

    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    yield;
    echo 'run to code line: ' . __LINE__ . PHP_EOL;

    return 4;
}

function yield_func2($is = false)
{
    $re = 0;
    try {
        echo 'run to code line: ' . __LINE__ . PHP_EOL;
        $re = yield 'exception';
        echo 'run to code line: ' . __LINE__ . PHP_EOL;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    echo 're : ' . $re . PHP_EOL;
    return '32';
}

function yield_func3($is = false)
{
    $re = -1;
    $arr = array(1,2,3);

    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    if ($is) {
        $re = yield;
    }
    echo 'run to code line: ' . __LINE__ . PHP_EOL;

    return $re;
}

function yield_func4()
{
    $arr = array(1,2,3);

    if (false) {
        yield;
    }
    echo 'run to code line: ' . __LINE__ . PHP_EOL;

    return 44;
}

function yield_func5()
{
    $arr = array(1,2,3);

    $arr = array('aa','bb','cc');
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    yield yield_func1();
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    return null;
}

function yield_func6()
{
    $arr = array(1,2,3);

    $gen = yield_func1();
    if ($gen instanceof Generator) {
        $gen->send(yield);
    }
    return null;
}

function yield_func7()
{
    $arr = array('aa','bb','cc');
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    yield $arr[1];
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    return 'dd';
}

function yield_func8()
{
    $arr = array('aa','bb','cc');
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    yield from $arr;
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    return null;
}

function yield_func9()
{
    $arr = array('aa','bb','cc');
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    $re = yield yield_func1();
    echo 'get yield return : ';
    var_export($re);
    echo PHP_EOL;
}

function yield_func10 ()
{
    yield 2;
    yield 'key' => 'value';
    yield 7 => 'cc';
    yield 5;
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    return 'yield return 10';
}


function yield_func11()
{
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    eval('$a = yield 10;');
//    eval('function yield_func12() {$a = yield 10;}; $gen = yield_func12();');
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    return $a;
}

function yield_func12()
{
    $result = array();
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    $result[] = yield 12;
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    $result[] = yield;
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    $result[] = yield;
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    return $result;
}

function yield_func13()
{
    try {
        echo 'run to code line: ' . __LINE__ . PHP_EOL;
        $re = yield 'exception';
        echo 'run to code line: ' . __LINE__ . PHP_EOL;
    } catch (Exception $e) {
        echo 'exception 1: ' . $e->getMessage() . PHP_EOL;
    }

    try {
        echo 'run to code line: ' . __LINE__ . PHP_EOL;
        $re = yield 'exception';
        echo 'run to code line: ' . __LINE__ . PHP_EOL;
    } catch (Exception $e) {
        echo 'exception 2:' . $e->getMessage() . PHP_EOL;
    }

    echo 're : ' . $re . PHP_EOL;
    return '32';
}

function yield_func14 ()
{
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
    yield 2 => 'aa';
    echo 'run to code line: ' . __LINE__ . PHP_EOL;
}


class YieldClass1
{
    public function yield_method1()
    {
        echo 'run to code line: ' . __LINE__ . PHP_EOL;
        yield;
        echo 'run to code line: ' . __LINE__ . PHP_EOL;
    }

    static public function yield_method2()
    {
        echo 'run to code line: ' . __LINE__ . PHP_EOL;
        yield;
        echo 'run to code line: ' . __LINE__ . PHP_EOL;
    }
}

function yield_func20 ()
{
    $arr = array();
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . PHP_EOL;
    $arr[] = yield 2;
    $arr[] = yield 'key' => 'value';
    $arr[] = yield 7 => 'cc';
    $arr[] = yield 5;
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . ', arr re: ';
    var_export($arr);
    echo PHP_EOL;
}

function yield_func21 ()
{
    $gen = yield_func20();
    $arr = array();
    $arr[] = $re = yield $gen->current();
    $arr[] = yield $gen->send($re);
    $arr[] = yield $gen->send($re);
    $arr[] = yield $gen->send($re);
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . ', arr re: ';
    var_export($arr);
    echo PHP_EOL;
    return 211;
}

function yield_func22 ()
{
    $gen = yield_func20();
    $arr = array();
    foreach ($gen as $key => $value) {
        $arr[] = yield $value;
    }
    echo 'run to function ' . __FUNCTION__ . ' line: ' . __LINE__ . ', arr re: ';
    var_export($arr);
    echo PHP_EOL;
}